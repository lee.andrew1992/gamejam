﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpawnerLogic : MonoBehaviour
{
	public GameObject bulletPrefab;
	public int orderIndex;

    public void FireBullet()
	{
		var bullet = GameObject.Instantiate(bulletPrefab, GameLogic.Instance.allBullets);
		bullet.transform.position = this.transform.position;
		bullet.transform.position = new Vector3(bullet.transform.position.x, 0, bullet.transform.position.z);
		var bulletLogic = bullet.GetComponent<BulletLogic>();
		bulletLogic.enabled = true;
	}
}
