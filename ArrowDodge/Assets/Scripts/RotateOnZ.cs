﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateOnZ : MonoBehaviour
{
    public float speed;

    // Update is called once per frame
    void Update()
    {
        this.gameObject.transform.Rotate(0, 0, Time.deltaTime * speed);
    }
}
