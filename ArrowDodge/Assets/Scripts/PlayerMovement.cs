﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	public float speed;
    public SpriteRenderer gameRing;
	
	private Vector3 startPos;

	private void Start()
	{
		this.transform.position = GameLogic.Instance.centerPos;
		startPos = GameLogic.Instance.centerPos;
	}

	// Update is called once per frame
	void Update()
    {
		this.transform.position = new Vector3(
			this.transform.position.x + (GetInput().x * speed * Time.deltaTime), 
			this.transform.position.y, 
			this.transform.position.z + (GetInput().y * speed * Time.deltaTime));

		float dist = Vector3.Distance(transform.position, startPos);

		if (dist > GameLogic.Instance.ringRadius)
		{
			Vector3 fromOriginToObject = transform.position - startPos;
			fromOriginToObject *= GameLogic.Instance.ringRadius / dist;
			transform.position = startPos + fromOriginToObject;
		}
	}

	Vector2 GetInput()
	{
		if (GameLogic.Instance.playerAlive)
		{
#if UNITY_EDITOR
			return new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).normalized;
#elif UNITY_ANDROID
			return new Vector2(Input.acceleration.x, Input.acceleration.y).normalized;
			
#endif

		}
		return Vector2.zero;
	}
}
