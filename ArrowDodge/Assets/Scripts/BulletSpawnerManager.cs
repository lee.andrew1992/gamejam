﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpawnerManager : MonoBehaviour
{
    public int spawnerCount;
    public int initialSpawnCount;
    public BulletSpawnManager bulletSpawnManager;

    public GameObject bulletSpawnerPrefab;
    public List<GameObject> spawners;

    private const float gameWorldRadius = 3.5f;
    private HashSet<int> spawnedIndices;

    private float timePassed;
    public float createMoreSpawnerInterval;

    void OnEnable()
    {
        var allCoords = CalculateSpawnerCoordinates();
        CreateSpawners(allCoords);
    }

	private void Start()
	{
        spawnedIndices = new HashSet<int>();

        for (int index = 0; index < initialSpawnCount; index++)
        { 
            ActivateRandomSpawner();
        }
    }

	private void Update()
	{
        if (bulletSpawnManager.activeSpawners.Count < spawnerCount)
        { 
            timePassed += Time.deltaTime;

            if (timePassed >= createMoreSpawnerInterval)
            {
                ActivateRandomSpawner();
                timePassed = 0;
            }
        }
	}

	List<Vector2> CalculateSpawnerCoordinates()
    {
        var allCoords = new List<Vector2>();

        for (int index = 0; index < spawnerCount; index++)
        {
            var theta = (2 * (index + 1) * Mathf.PI) / spawnerCount;
            var coord = new Vector2(gameWorldRadius * Mathf.Cos(theta), gameWorldRadius * Mathf.Sin(theta));

            allCoords.Add(coord);
        }

        return allCoords;
    }

    public void ActivateRandomSpawner()
    {
        var randomIndex = Random.Range(0, spawnerCount);
        while (spawnedIndices.Contains(randomIndex))
        { 
            randomIndex = Random.Range(0, spawnerCount);
        }

        ActivateSpawner(randomIndex);
    }

    public void ActivateSpawner(int index)
    {
        BulletSpawnerLogic logic = spawners[index].GetComponent<BulletSpawnerLogic>();
        logic.enabled = true;

        bulletSpawnManager.activeSpawners.Add(spawners[index]);
        spawnedIndices.Add(index);
    }

    void CreateSpawners(List<Vector2> coords)
    {
        var index = 1;

        foreach (Vector2 coord in coords)
        {
            var spawner = GameObject.Instantiate(bulletSpawnerPrefab);
            spawner.transform.parent = this.transform;
            spawner.transform.localPosition = coord;
            spawner.name = bulletSpawnerPrefab.name + index;
            spawner.GetComponent<BulletSpawnerLogic>().orderIndex = index;
            index++;

            spawners.Add(spawner);
        }
    }
}
