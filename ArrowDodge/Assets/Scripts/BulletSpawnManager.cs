﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BulletSpawnManager : MonoBehaviour
{
    public List<GameObject> activeSpawners;

	private float allShotsTimePassed;
	private float defaultShotsTimePassed;
	private float spiralShotsTimePassed;

	private void OnEnable()
	{
		activeSpawners = new List<GameObject>();
	}

	// Update is called once per frame
	void Update()
    {
		if (!GameLogic.Instance.playerAlive)
		{
			return;
		}

		if (GameLogic.Instance.firstShotsFired)
		{
			if (spiralShotsTimePassed >= GameLogic.Instance.spiralShotsFireInterval)
			{
				StartCoroutine(ShootSpiralShots());
			}
			else
			{ 
				if (allShotsTimePassed >= GameLogic.Instance.allBulletsFireInterval)
				{
					ShootAllShots();
				}
				if (defaultShotsTimePassed >= GameLogic.Instance.defaultFireInterval)
				{
					ShootDefaultShots();
				}
			}
		}
		else
		{
			if (allShotsTimePassed >= GameLogic.Instance.firstShotTimer)
			{
				ShootAllShots();
				GameLogic.Instance.firstShotsFired = true;
			}
		}

		spiralShotsTimePassed += Time.deltaTime;
		defaultShotsTimePassed += Time.deltaTime;
		allShotsTimePassed += Time.deltaTime;
	}

	void ShootAllShots()
	{
		foreach (GameObject spawner in activeSpawners)
		{
			spawner.GetComponent<BulletSpawnerLogic>().FireBullet();
		}
		allShotsTimePassed = 0;
	}

	void ShootDefaultShots()
	{
		for (int index = 0; index < activeSpawners.Count / 2; index++)
		{
			activeSpawners[index].GetComponent<BulletSpawnerLogic>().FireBullet();
		}
		defaultShotsTimePassed = 0;
	}

	IEnumerator ShootSpiralShots()
	{
		spiralShotsTimePassed = 0;

		var sortedActiveSpawners = activeSpawners.OrderBy(s => s.GetComponent<BulletSpawnerLogic>().orderIndex);
		foreach (GameObject spawner in sortedActiveSpawners)
		{
			defaultShotsTimePassed = 0;
			allShotsTimePassed = 0;

			spawner.GetComponent<BulletSpawnerLogic>().FireBullet();
			yield return new WaitForSeconds(0.05f);
		}
	}
}
