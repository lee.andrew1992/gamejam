﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Scoreboard : MonoBehaviour
{
    public TextMeshProUGUI scoreboardUI;
    public float mspaceValue;
    private float score;

    private const string monospaceTags = "<mspace={0}em>{1}</mspace>";

    // Update is called once per frame
    void Update()
    {
        if (GameLogic.Instance.playerAlive)
        { 
            score += (Time.deltaTime * 10);
            scoreboardUI.text = string.Format(monospaceTags, mspaceValue, ((int)score).ToString());
        }
    }
}
