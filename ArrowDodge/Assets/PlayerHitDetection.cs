﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHitDetection : MonoBehaviour
{
	public int bulletLayer;
	public GameObject bombEffectPrefab;

	private void OnCollisionEnter(Collision collision)
	{
		if (collision.collider.gameObject.layer == bulletLayer)
		{
			GameLogic.Instance.GetComponent<RestartPrompt>().enabled = true;
			GameLogic.Instance.playerAlive = false;
			this.transform.parent.gameObject.SetActive(false);

			var bombEffect = GameObject.Instantiate(bombEffectPrefab);
			bombEffect.transform.position = this.transform.position;
			bombEffect.GetComponent<BombEffectController>().enabled = true;
		}
	}
}
