﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLogic : MonoBehaviour
{
    public Transform allBullets;
    public float bulletLifeTime;
    public float bulletSpeed;
    public float bulletInaccuracy;
    public float firstShotTimer;
    public float allBulletsFireInterval;
    public float defaultFireInterval;
    public float spiralShotsFireInterval;

    public Vector3 centerPos = Vector3.zero;
    private const float ringRadiusBase = 2.2f; // perfect radius at screen aspect 16:10

    public bool playerAlive;
    public float ringRadius { get; private set; }

    public bool firstShotsFired = false;

    private static GameLogic instance;
    public static GameLogic Instance
    {
        get { return instance; }
    }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            playerAlive = true;            
        }

        CalculateRingRadius();
    }

    void CalculateRingRadius()
    {
        ringRadius = ringRadiusBase * Camera.main.aspect;
    }
}
