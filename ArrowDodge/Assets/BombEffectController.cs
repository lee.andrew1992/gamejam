﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombEffectController : MonoBehaviour
{
    public SpriteRenderer renderer;

    // Update is called once per frame
    void Update()
    {
        this.gameObject.transform.localScale += Vector3.one * Time.deltaTime;
        var fadeValue = renderer.material.GetFloat("_Fade");
        renderer.material.SetFloat("_Fade", fadeValue - (Time.deltaTime / 1.1f));
    }
}
