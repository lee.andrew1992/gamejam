﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletLogic : MonoBehaviour
{
    private float lifeTime;
    private float timePassed;

    private float speed;
    private float inAccuracy;

    private bool launched;

    private Vector3 direction;

    void Start()
    {
        Launch();
    }

	// Update is called once per frame
	void Update()
    {
        if (launched)
        { 
            if (timePassed >= lifeTime)
            {
                GameObject.Destroy(this.gameObject);
            }
            else
            {
                this.transform.position -= direction * speed * Time.deltaTime;

                timePassed += Time.deltaTime;
            }
        }
    }

    private void Launch()
	{
        lifeTime = GameLogic.Instance.bulletLifeTime;
		speed = GameLogic.Instance.bulletSpeed;
        inAccuracy = GameLogic.Instance.bulletInaccuracy;
        SetDirection();
        this.transform.LookAt(this.transform.position + direction);

		launched = true;
	}

    private void SetDirection()
    {
        var currInAccuracy = new Vector3(Random.Range(-inAccuracy, inAccuracy), 0, Random.Range(-inAccuracy, inAccuracy));
        var dir = this.transform.position - GameLogic.Instance.centerPos + currInAccuracy;
        direction = new Vector3(dir.x, 0, dir.z).normalized;
    }
}
