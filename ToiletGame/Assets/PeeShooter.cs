﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeeShooter : MonoBehaviour
{
    public GameObject peeDropletPrefab;
    public GameObject destination;
    public GameObject peeParent;
    public UnityEngine.UI.Slider peeMeter;

    const float shootDelay = 0.01f;
    float currShootDelay = 0;

    public void ShootPee()
    {
        if (GameManager.Instance.peeReserve <= 0)
        {
            return;
        }

        GameManager.Instance.peeReserve -= Time.deltaTime * 30;
        var direction = (destination.transform.position - this.transform.position).normalized;
        var droplet = GameObject.Instantiate(peeDropletPrefab, peeParent.transform);
        droplet.transform.position = this.transform.position;
        droplet.GetComponent<PeeDropletLogic>().Initialize(direction);

        currShootDelay = 0;
    }

    private void Update()
    {
        peeMeter.value = GameManager.Instance.peeReserve / 100;
        
        if (peeMeter.value <= 0)
        {
            GameManager.Instance.restartButton.gameObject.SetActive(true);
        }

        if (currShootDelay > shootDelay)
        { 
		    if (Input.touchCount > 0 || Input.GetMouseButton(0))
		    {
                ShootPee();
		    }
        }
        currShootDelay += Time.deltaTime;
    }
}
