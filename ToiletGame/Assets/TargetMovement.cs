﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetMovement : MonoBehaviour
{
    public int speed = 1000;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = new Vector3(
            Mathf.Clamp(this.transform.position.x + (GetInput().x * speed * Time.deltaTime), Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).x, Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0)).x),
            Mathf.Clamp(this.transform.position.y + (GetInput().y * speed * Time.deltaTime), Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).y, Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height, 0)).y),
            this.transform.position.z);
    }

    Vector2 GetInput()
    {

#if UNITY_EDITOR
        Debug.Log(Input.GetAxisRaw("Horizontal") + " | " + Input.GetAxisRaw("Vertical"));
            return new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).normalized;
#elif UNITY_ANDROID
			return new Vector2(Input.acceleration.x, Input.acceleration.y).normalized;	
#endif
    }
}
