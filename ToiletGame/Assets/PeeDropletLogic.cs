﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeeDropletLogic : MonoBehaviour
{
	public static bool initialized;
	Vector3 direction;

	public void Initialize(Vector3 direction)
	{
		initialized = true;
		this.direction = direction;
	}

	// Update is called once per frame
	void Update()
    {
		if (initialized)
		{
			this.transform.position += this.direction * Time.deltaTime * 1000;
		}
    }
}
