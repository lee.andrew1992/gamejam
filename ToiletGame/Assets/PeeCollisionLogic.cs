﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeeCollisionLogic : MonoBehaviour
{
	const float lifeTime = 2f;
	float currLifeTime = 0;

	private void Update()
	{
		currLifeTime += Time.deltaTime;
		if (currLifeTime > lifeTime)
		{
			GameObject.Destroy(this.transform.parent.gameObject);
		}

		if (this.transform.position.z > 0) 
		{
			if (GameManager.Instance.waterCollider.bounds.Contains(this.transform.position))
			{
				GameManager.Instance.score += 1;
			}
			else if (GameManager.Instance.bowlCollider.bounds.Contains(this.transform.position))
			{
				GameManager.Instance.score += 5;
			}
			else
			{
				GameManager.Instance.score -= 1;
			}
			GameObject.Destroy(this.transform.parent.gameObject);
		}
	}
}
