﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrunkCameraEffect : MonoBehaviour
{
    bool rotatingX;
    bool rotatingY;
    bool zooming;

    float lastLerpValueX;
    float lastLerpValueY;
    bool lastZoomIn;

    const int zoomInMax = 800;
    const int zoomOutMax = 1200;

    enum Rotation
    { 
        x,
        y
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!rotatingX)
        {
            CallLerp(Rotation.x);
        }

        if (!rotatingY)
        {
            CallLerp(Rotation.y);
        }

        if (!zooming)
        {
            StartCoroutine(ZoomInOut(lastZoomIn, Random.Range(0.5f, 1.5f)));
        }
    }

    IEnumerator LerpRotate(Rotation rotation, float duration, float rotationValue)
    {
        float currDuration = 0;
        while (currDuration < duration)
        {
            ToggleRotationStatus(rotation, true);
            currDuration += Time.deltaTime;
            switch (rotation)
            {
                case Rotation.x:
                    lastLerpValueX = rotationValue;
                    this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(rotationValue, 0, 0), Time.deltaTime / 4);
                break;

                case Rotation.y:
                    lastLerpValueY = rotationValue;
                    this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(0, rotationValue, 0), Time.deltaTime / 4);
                    break;
            }
            yield return null;
        }
        ToggleRotationStatus(rotation, false);
    }

    IEnumerator ZoomInOut(bool zoomIn, float duration)
    {
        float currDuration = 0;
        while (currDuration < duration)
        {
            zooming = true;
            if (zoomIn)
            {
                Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, zoomInMax, Time.deltaTime / 2);
            }
            else
            {
                Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, zoomOutMax, Time.deltaTime / 2);
            }

            currDuration += Time.deltaTime;
            yield return null;
        }
        lastZoomIn = !zoomIn;
        zooming = false;
    }

    void CallLerp(Rotation rotation)
    {
        var lerpValue = Random.Range(15.0f, 25.0f);

        switch (rotation)
        {
            case Rotation.x:
                if (lastLerpValueX > 0)
                {
                    lerpValue *= -1;
                }
                break;
            
            case Rotation.y:
                if (lastLerpValueY > 0)
                {
                    lerpValue *= -1;
                }
                break;
        }

        var duration = Random.Range(1f, 2f);
        StartCoroutine(LerpRotate(rotation, duration, lerpValue));
    }

    void ToggleRotationStatus(Rotation rotation, bool status)
    {
        switch (rotation)
        {
            case Rotation.x:
                rotatingX = status;
                break;

            case Rotation.y:
                rotatingY = status;
                break;
        }
    }
}
